#!/bin/sh

html='boot@'

if [ ! -d doc/html ]; then
    mkdir -p -m 0755 doc/html || exit 1
fi

for i in ${html};do
     lowdown -s doc/${i}.md -o doc/html/${i}.html || exit 1
done

exit 0
