#
# This Makefile requires GNU make.
#
# Do not make changes here.
# Use the included .mak files.
#

make_need := 3.81
ifeq "" "$(strip $(filter $(make_need), $(firstword $(sort $(make_need) $(MAKE_VERSION)))))"
fail := $(error Your make ($(MAKE_VERSION)) is too old. You need $(make_need) or newer)
endif

-include config.mak
include package/targets.mak

LOWDOWN := $(shell type -p lowdown)
ifdef LOWDOWN
GENERATE_HTML := $(shell doc/make-html.sh)
GENERATE_MAN := $(shell doc/make-man.sh)
endif
INSTALL_HTML := $(wildcard doc/html/*.html)
INSTALL_MAN := $(wildcard doc/man/*/*)
INSTALL := ./tools/install.sh

install: install-module install-configure install-script install-rc-local install-seed install-html install-man
install-module: $(MODULE_TARGET:module/%=$(DESTDIR)$(service_directory)/$(package)/%)
install-configure: $(MODULE_CONFIGURE_TARGET:module/configure/configure=$(DESTDIR)$(service_directory)/$(package)/configure/configure)
install-script: $(SCRIPT_TARGET:module/configure/crypt.awk=$(DESTDIR)$(script_directory)/crypt.awk)
install-rc-local: $(SKEL_SCRIPT_TARGET:module/configure/rc.local=$(DESTDIR)$(skel_directory)/rc.local)
install-seed: $(SEED_TARGET:module/configure/boot=$(DESTDIR)$(seed_directory)/boot)
install-html: $(INSTALL_HTML:doc/html/%.html=$(DESTDIR)$(datarootdir)/doc/$(package_doc)/%.html)
install-man: install-man1
install-man1: $(INSTALL_MAN:doc/man/man1/%.1=$(DESTDIR)$(mandir)/man1/%.1)

$(DESTDIR)$(service_directory)/$(package)/%: module/%
	exec $(INSTALL) -D -m 644 $< $@
	sed -i -e "s,@adm_conf@,$(adm_conf)," \
		-e "s,@script_directory@,$(script_directory)," \
		-e "s,@skel_directory@,$(skel_directory)," \
		-e "s,@livedir@,$(livedir)," \
		-e "s,@tmpfiles_path@,$(tmpfiles)," \
		-e "s,@modules_path@,$(modules)," \
		-e "s,@VERSION@,$(version)," \
		-e "s,@HOSTNAME@,$(HOSTNAME)," \
		-e "s,@HARDWARECLOCK@,$(HARDWARECLOCK)," \
		-e "s,@TZ@,$(TZ)," \
		-e "s,@SETUPCONSOLE@,$(SETUPCONSOLE)," \
		-e "s,@TTY@,$(TTY)," \
		-e "s,@KEYMAP@,$(KEYMAP)," \
		-e "s,@FONT@,$(FONT)," \
		-e "s,@FONT_MAP@,$(FONT_MAP)," \
		-e "s,@FONT_UNIMAP@,$(FONT_UNIMAP)," \
		-e "s,@UDEV@,$(UDEV)," \
		-e "s,@SYSCTL@,$(SYSCTL)," \
		-e "s,@FORCECHCK@,$(FORCECHCK)," \
		-e "s,@LOCAL@,$(LOCAL)," \
		-e "s,@CONTAINER@,$(CONTAINER)," \
		-e "s,@TMPFILE@,$(TMPFILE)," \
		-e "s,@MODULE_KERNEL@,$(MODULE_KERNEL)," \
		-e "s,@MODULE_SYSTEM@,$(MODULE_SYSTEM)," \
		-e "s,@RANDOMSEED@,$(RANDOMSEED)," \
		-e "s,@FSTAB@,$(FSTAB)," \
		-e "s,@SWAP@,$(SWAP)," \
		-e "s,@LVM@,$(LVM)," \
		-e "s,@DMRAID@,$(DMRAID)," \
		-e "s,@MDRAID@,$(MDRAID)," \
		-e "s,@BTRFS@,$(BTRFS)," \
		-e "s,@ZFS@,$(ZFS)," \
		-e "s,@ZFS_IMPORT@,$(ZFS_IMPORT)," \
		-e "s,@CRYPTTAB@,$(CRYPTTAB)," \
		-e "s,@FIREWALL@,$(FIREWALL)," \
		-e "s,@CGROUPS@,$(CGROUPS)," \
		-e "s,@MNT_PROC@,$(MNT_PROC)," \
		-e "s,@MNT_SYS@,$(MNT_SYS)," \
		-e "s,@MNT_DEV@,$(MNT_DEV)," \
		-e "s,@MNT_RUN@,$(MNT_RUN)," \
		-e "s,@MNT_TMP@,$(MNT_TMP)," \
		-e "s,@MNT_PTS@,$(MNT_PTS)," \
		-e "s,@MNT_SHM@,$(MNT_SHM)," \
		-e "s,@MNT_NETFS@,$(MNT_NETFS)," \
		-e "s,@POPULATE_SYS@,$(POPULATE_SYS)," \
		-e "s,@POPULATE_DEV@,$(POPULATE_DEV)," \
		-e "s,@POPULATE_RUN@,$(POPULATE_RUN)," \
		-e "s,@POPULATE_TMP@,$(POPULATE_TMP)," \
		-e "s,@CHECK_CONFIGURATION@,$(CHECK_CONFIGURATION)," $@

$(DESTDIR)$(service_directory)/$(package)/configure/configure: module/configure/configure
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

$(DESTDIR)$(script_directory)/crypt.awk: module/configure/crypt.awk
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

$(DESTDIR)$(skel_directory)/rc.local: module/configure/rc.local
	exec $(INSTALL) -D -m 755 $< $@

$(DESTDIR)$(seed_directory)/boot: module/configure/boot
	exec $(INSTALL) -D -m 644 $< $@

$(DESTDIR)$(datarootdir)/doc/$(package_doc)/%.html: doc/html/%.html
	$(INSTALL) -D -m 644 $< $@ && \
	sed -e 's,%%skel_directory%%,$(skel_directory),g' $< > $@

$(DESTDIR)$(mandir)/man1/%.1: doc/man/man1/%.1
	$(INSTALL) -D -m 644 $< $@ && \
	sed -e 's,%%skel_directory%%,$(skel_directory),g' $< > $@

version:
	@echo $(version)

.PHONY: install version install-html install-man

.DELETE_ON_ERROR:
