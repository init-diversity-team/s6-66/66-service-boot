[main]
@type = module
@version = @VERSION@
@description = "Set of services to boot a machine"
@intree = boot
@user = ( root )

[regex]
@infiles = (
::@bootconf@=@adm_conf@/boot@@I/version
::@scripts@=@script_directory@
::@initconf@=@skel_directory@/init.conf
::@skeldir@=@skel_directory@
::@live_dir@=@livedir@
::@tmpfiles@=@tmpfiles_path@
::@modules@=@modules_path@
::@vers@=@VERSION@
)

[environment]
# Copyright (c) 2015-2023 Eric Vidal <eric@obarun.org>
# All rights reserved.
#
# This file is part of Obarun. It is subject to the license terms in
# the LICENSE file found in the top-level directory of this
# distribution.
# This file may not be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#
# System configuration file for a boot process running and handled
# by 66 suite program.
#
# Do not use quotes or double-quotes on this file
#
# The '!' is used to not export the variable on the environment.
# If you want to keep it, remove the exclamation mark '!'.
#
#========================== WARNING ====================================
#
# Do not forget to configure again the boot@ module after editing
# this file to apply your changes at the next reboot e.g:
# 66 reconfigure boot@system
#
#======================= Global Settings ===============================

## Set your HOSTNAME.

@HOSTNAME@

## Set your timezone, available timezones can be found at /usr/share/zoneinfo.

@TZ@

## Set the number of tty(s) to start.
## 0 means no tty. Maximum is 11.

@TTY@

## Console keymap to load, see loadkeys(8).

@KEYMAP@

## Console font to load, see setfont(8).

@FONT@

## Console map to load, see setfont(8).

@FONT_MAP@

## Console unimap to load, see setfont(8).

@FONT_UNIMAP@

#========================== Devices ====================================

## Mount devices from FSTAB file [yes|no].

@FSTAB@

## Activate swap [yes|no].

@SWAP@

## Activate lvm volume groups [yes|no].

@LVM@

## Activate dmraid disks [yes|no].

@DMRAID@

## Activate btrfs devices [yes|no].

@BTRFS@

## Mount zfs filesystems [yes|no].

@ZFS@

## Zpool Import method [scan|zpoolcache]
## It has no effects if ZFS is set to no or commented.

@ZFS_IMPORT@

## Activate mdraid arrays

@MDRAID@

#========================== Configuration checks ==========================

## Check the configuration for errors

@CHECK_CONFIGURATION@

#=======================================================================
#=================== Advanced user configuration =======================
#
# If you don't know what the following variables do, you
# don't need to change them.  The machine will still boot
# with the default values
#
#=======================================================================

#====================== System configuration ===========================

## Setup the console
## If set to no, KEYMAP,FONT,FONT_MAP,FONT_UNIMAP has no effects

@SETUPCONSOLE@

## Set RTC [UTC|localtime].

@HARDWARECLOCK@

## Use udev [yes|no]
## If set to no the following variables has no effects or set to no:
## SETUPCONSOLE, KEYMAP, FONT, FONT_MAP, FONT_UNIMAP, CRYPTTAB, DMRAID,
## BTRFS, LVM

@UDEV@

## Kernel configuration with sysctl [yes|no].

@SYSCTL@

## Force a check of filesystem partition [yes|no].

@FORCECHCK@

## Use rc.local script [yes|no].

@LOCAL@

## Boot inside a container [yes|no].
## If set to yes the following variables have no effect or are set to no:
## HARDWARECLOCK, SETUPCONSOLE, KEYMAP, FONT, FONT_MAP, FONT_UNIMAP, CRYPTTAB
## SWAP, LVM, DMRAID, BTRFS, ZFS, UDEV, SYSCTL, FORCECHCK, CGROUPS
## MODULE_SYSTEM, RANDOMSEED, MNT_NETFS

@CONTAINER@

## Apply tmpfiles.d configuration file [yes|no].

@TMPFILE@

## Load kernel modules [yes|no].

@MODULE_KERNEL@

## Load modules from modules.d files

@MODULE_SYSTEM@

## Populate ramdom seed

@RANDOMSEED@

#========================== Security ===================================

## Activate encrypted devices [yes|no].

@CRYPTTAB@

## Firewall program to use [iptables|ip6tables|nftables|ebtables|arptables]
## Comment to not use any firewall at all.

@FIREWALL@

#========================== Pseudo filesystem ==========================

## Mount cgroups [yes|no].

@CGROUPS@

## Mount /proc directory [yes|no].

@MNT_PROC@

## Mount /sys directory [yes|no].

@MNT_SYS@

## Mount /dev directory [yes|no].

@MNT_DEV@

## Mount /run directory [yes|no].

@MNT_RUN@

## Mount /tmp directory [yes|no].

@MNT_TMP@

## Mount /dev/pts directory [yes|no].
## Only valuable if MNT_DEV is set to yes.

@MNT_PTS@

## Mount /dev/shm directory [yes|no].
## Only valuable if MNT_DEV is set to yes.

@MNT_SHM@

## Mount no-network filesystem [yes|no].
## Fstype mounted is: nosysfs,nonfs,nonfs4,nosmbfs,nocifs

@MNT_NETFS@

## Create and mount /sys/firmware/efi/efivars, /sys/fs/fuse/connections,
## /sys/kernel/{config,debug,security} and /sys/kernel/debug/tracing
## if exist at /proc/filesystems

@POPULATE_SYS@

## Create and mount /dev/{hugepages,mqueue}
## if exist at /proc/filesystems

@POPULATE_DEV@

## Create /run/{lvm,user,lock,utmp} directories

@POPULATE_RUN@

## Create /tmp/{.X11-unix,.ICE-unix} files

@POPULATE_TMP@



